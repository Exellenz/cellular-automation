﻿using System.Windows;

namespace CellularAutomata
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private Automata _automata;

        public MainWindow()
        {
            InitializeComponent();
        }


        private void StartAutomata(object sender, RoutedEventArgs e)
        {
            if (_automata != null)
            {
                return;
            }

            var ruleNumber = int.Parse(RuleNumber.Text);
            var ruleColoring = RuleColoring.IsChecked != null && (bool) RuleColoring.IsChecked;

            _automata = new Automata(ruleNumber, 151, 101)
            {
                Drawer = new AutomataDrawer(CellWrapper, (Style)FindResource("AutomataCell"))
            };
            CellWrapper.Children.Clear();
            _automata.StartProcess(ruleColoring, 50);
        }
    }
}
