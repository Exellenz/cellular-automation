﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CellularAutomata
{
    public class AutomataDrawer
    {
        private readonly WrapPanel _cellWrapper;
        private readonly Style _cellStyle;
        private int _columnsNumber;
        private readonly Color[] _ruleColors;

        public AutomataDrawer(WrapPanel cellWrapper, Style cellStyle)
        {
            _cellWrapper = cellWrapper;
            _cellStyle = cellStyle;
            _ruleColors = new[]
            {
                Colors.GreenYellow,
                Colors.YellowGreen,
                Colors.LimeGreen,
                Colors.MediumSeaGreen,
                Colors.ForestGreen,
                Colors.SeaGreen,
                Colors.DarkOliveGreen,
                Colors.Black
            };
        }

        public void SetAutomataGrid(int columns, int rows)
        {
            _columnsNumber = columns;

            int cellWidth = 0, cellHeight = 0;

            foreach (var setterBase in _cellStyle.Setters)
            {
                var setter = (Setter) setterBase;
                switch (setter.Property.Name)
                {
                    case "Width":
                        cellWidth = int.Parse(setter.Value.ToString());
                        break;
                    case "Height":
                        cellHeight = int.Parse(setter.Value.ToString());
                        break;
                }
            }

            _cellWrapper.Width = columns * cellWidth;
            _cellWrapper.Height = rows * cellHeight;

            for (var i = 0; i < columns*rows; i++)
            {
                _cellWrapper.Children.Add(new Rectangle
                {
                    Style = _cellStyle
                });
            }
        }

        public void AddCell(int row, int column, int cellCode)
        {
            cellCode = Math.Min(cellCode, _ruleColors.Length - 1);
            ((Rectangle) _cellWrapper.Children[GetCellIndex(row, column)]).Fill =
                    new SolidColorBrush(
                        cellCode == Automata.EMPTY_CELL ? Colors.White : _ruleColors[cellCode]
                        );
        }

        private int GetCellIndex(int row, int column)
        {
            return row * _columnsNumber + column;
        }
    }
}