﻿using System;
using System.Linq;

namespace CellularAutomata
{
    public class RulePattern
    {
        private readonly int _left;
        private readonly int _center;
        private readonly int _right;

        public int CellValue { get; private set; }

        public RulePattern(int patternValue, int cellValue)
        {
            CellValue = cellValue;

            var bits = Convert.ToString(patternValue, 2).PadLeft(3, '0').ToArray();
            _left = int.Parse(bits[0].ToString());
            _center = int.Parse(bits[1].ToString());
            _right = int.Parse(bits[2].ToString());
        }

        public bool PatternMatches(int leftCell, int centerCell, int rightCell)
        {
            return
                TransformCodeToBoolean(leftCell) == _left &&
                TransformCodeToBoolean(centerCell) == _center &&
                TransformCodeToBoolean(rightCell) == _right;
        }

        private int TransformCodeToBoolean(int code)
        {
            return code == Automata.EMPTY_CELL ? 0 : 1;
        }
    }
}