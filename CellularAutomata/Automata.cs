﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CellularAutomata
{
    public class Automata
    {
        private readonly List<RulePattern> _patterns = new List<RulePattern>();
        private readonly int _width;
        private readonly int _height;
        private int[,] _field;

        public const int EMPTY_CELL = -1;

        public AutomataDrawer Drawer;

        public Automata(int ruleNumber, int width, int height)
        {
            _width = width;
            _height = height;
            InitField(width, height);
            SetRule(ruleNumber);
        }

        private void InitField(int width, int height)
        {
            _field = new int[_height, _width];
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    _field[i, j] = EMPTY_CELL;
                }
            }
            
            _field[0, width/2] = 1;
        }

        private void SetRule(int ruleNumber)
        {
            var newCells = Convert.ToString(ruleNumber, 2).PadLeft(8, '0').Reverse().ToArray();
            for (var i = 0; i <= 7; i++)
            {
                var nextIsEmpty = newCells[i] == '0';
                var rulePattern = new RulePattern(i, nextIsEmpty ? EMPTY_CELL : i);
                _patterns.Add(rulePattern);
            }
        }

        public async void StartProcess(bool ruleColors, int delay, int repeats = 50)
        {
            Drawer.SetAutomataGrid(_width, _height);

            for (var i = 0; i < _height*repeats; i++)
            {
                var row = i % _height;
                var nextRow = (i + 1) % _height;
                for (var column = 0; column < _width; column++)
                {
                    Drawer.AddCell(row, column, _field[row,column]);

                    var leftCell = _field[row, (column - 1 + _width) % _width];
                    var centerCell = _field[row, column];
                    var rightCell = _field[row, (column + 1) % _width];
                    var newCell = EMPTY_CELL;

                    foreach (var rulePattern in _patterns)
                    {
                        if (rulePattern.PatternMatches(leftCell, centerCell, rightCell))
                        {
                            newCell = rulePattern.CellValue;
                            break;
                        }
                    }

                    if (ruleColors)
                    {
                        _field[nextRow, column] = newCell;
                    }
                    else
                    {
                        if (newCell == EMPTY_CELL)
                        {
                            newCell = _field[nextRow, column] - 1;
                            _field[nextRow, column] = newCell < 0 ? EMPTY_CELL : newCell;
                        }
                        else
                        {
                            _field[nextRow, column] = Math.Min(_patterns.Count - 1, _field[nextRow, column] + 1);
                        }
                    }
                }
                await Task.Delay(delay);
            }
        }
    }
}